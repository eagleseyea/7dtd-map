#!/bin/bash
set -ex

gen() {
	local outdir="$1"
	local scalePercent="$2"

	[[ -d "$outdir" ]] && return

	mkdir -p "$outdir"
	pushd "$outdir"
	export INPUT="${ORIGINAL_INPUT}"
	if [[ ! -z "$scalePercent" ]]; then
		convert "${INPUT}" -scale ${scalePercent}% "./tmp.png"
		export INPUT="./tmp.png"
	fi
	convert -crop ${TILE}x${TILE} +repage "$INPUT" tiles_%d.png
	$SCRIPTDIR/rename.sh
	popd
}

SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
cd "$SCRIPTDIR"

export ORIGINAL_INPUT="$(readlink -f "public/map/full.png")"
export TILE="256"

if [[ "$1" == "--force" ]]; then
    rm -rf public/map/full
    rm -rf public/map/50
    rm -rf public/map/25
fi

gen public/map/full
gen public/map/50 50
gen public/map/25 25

