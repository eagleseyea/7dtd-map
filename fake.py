#!/usr/bin/env python

import os
from PIL import Image, ImageDraw

cfg = {
        'row_start': -40,
        'col_start': -40,
        'row_max': 40,
        'col_max': 40,
        'outdir': 'public/map/fake',
        'name': "map_r{row}_c{col}.png",
        }

if __name__ == '__main__':

    try:
        os.mkdir(cfg['outdir'])
    except FileExistsError:
        pass

    row = cfg['row_start']
    while row < cfg['row_max']:

        col = cfg['col_start']
        while col < cfg['col_max']:

            name = cfg['name'].format(row=row, col=col)
            img = Image.new('RGB', (256, 256), (255, 255, 255))
            d = ImageDraw.Draw(img)
            d.text((82,122), name, fill=(107, 10, 107))
            img.save(os.path.join(cfg['outdir'], name))

            col += 1
        row += 1

