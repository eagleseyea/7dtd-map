#!/bin/bash

set -ex

INPUT="${INPUT:-public/map/full.png}"
TILE="${TILE:-256}"

SIZE="$(identify "$INPUT" | sed -nr 's/.* ([0-9]+)x([0-9]+) .*/\1 \2/p')"
WIDTH="${SIZE/ *}"
HEIGHT="${SIZE/* }"

divideCeil() {
	# compenstate for floating point result of division by tile size
	# - there may be some pixels left on the right/bottom which will cause extra tile row/col
	local DIM="$1"
	local TILE="$2"
	local CNT="$(( $DIM / $TILE ))"
	if [[ "$(( $CNT * $TILE ))" -lt "$DIM" ]]; then
		echo "$(( $CNT + 1 ))"
	else
		echo "$CNT"
	fi
}

# calculate amount of tiles horizontally (cols) and vertically (rows)
TILE_COLS="$(divideCeil $WIDTH $TILE )"
TILE_ROWS="$(divideCeil $HEIGHT $TILE )"

# now we want to know starting counting point
# since we use centered map and so need to have x/y indexes negative
# for start lets just use negative half of one dimension (assumes same sizes)
# - in regular case where you start from top left as row=0/col=0 set offset to zero (0)
# - in case of centered it should be negative number used to add to row/col number for file name
OFFSET=${OFFSET:-$(( - ($TILE_COLS / 2) ))}

ROW=0
pwd
while [[ $ROW -lt $TILE_ROWS ]]; do
	COL=0
	while [[ $COL -lt $TILE_COLS ]]; do
		IDX=$(( ($ROW * $TILE_COLS) + $COL ))
		SRC="tiles_$IDX.png"
		DST="map_r$(( $ROW + $OFFSET ))_c$(( $COL + $OFFSET )).png"
		if [[ ! -f "$SRC" ]]; then
			echo "Missing file $SRC"
			exit 1
		fi
		mv "$SRC" "$DST"
		optipng "$DST"

		COL=$(( $COL + 1 ))
	done
	ROW=$(( $ROW + 1 ))
done
