#!/bin/bash

DST="$1"

if [[ "$DST" == "help" || "$DST" == "--help" || -z "$DST" ]]; then
	echo "You have to provide destination for rsync." >&2
	exit 1
fi

rsync -rvz --delete --exclude=map/fake --delete-excluded public/ "$1"
